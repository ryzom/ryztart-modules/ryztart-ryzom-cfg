import os
import platform
from setuptools import setup

extras_require = {}

install_requires = []


with open('README.md') as fh:
    long_description = fh.read()

setup(
    name='ryzom_cfg',
    author='Ulukyn',
    author_email='ulukyn@gmail.com',
    description=('Ryzom Cfg Module for Ryztart.'),
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/ryzom/ryztart-modules/ryztart-ryzom-cfg',
    download_url='https://gitlab.com/api/v4/projects/30709757/repository/archive.zip?sha=v1.0',
    keywords=[],
    install_requires=install_requires,
    extras_require=extras_require,
    version='1.0',
    include_package_data=True,
    packages=['ryzom_cfg'],
    package_dir={'ryzom_cfg': 'ryzom_cfg'},
    package_data={},
    license='New BSD license',
    classifiers=[
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Environment :: X11 Applications :: GTK',
        'Environment :: X11 Applications :: Qt',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Topic :: Software Development :: Libraries :: Application Frameworks',
        'Topic :: Software Development :: Libraries :: Python Modules',
        'Topic :: Software Development :: User Interfaces'
    ],
)
