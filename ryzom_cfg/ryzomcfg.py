import os
import sys

from kyssModule import KyssModule

class RyzomCfgFile(KyssModule):

	def __init__(self, filename, rootConfigFilename=""):
		KyssModule.__init__(self)
		if os.path.isfile(filename):
			with open(filename, "r") as f:
				content = f.read().split(";")
		else:
			content = []
		self.rootconfig = rootConfigFilename
		self.filename = filename
		self.raw_content = content
		self.content = self.parse(content, rootConfigFilename)
	
	def appendValue(self, value, values):
		value = value.strip()
		if value:
			if value[0] == "\"":
				values.append(value[1:-1])
			else:
				if "." in value:
					try:
						values.append(float(value))
					except:
						values.append(value)
				else:
					try:
						values.append(int(value))
					except:
						values.append(value)
	
	def parse(self, lines, rootConfigFilename=""):
		parameters = {}
		for line in lines:
			state = ""
			values = []
			value = ""
			var = ""
			first_slash = False
			start_of_string = False
			end_of_line = False
			append_list = False

			for c in line:
				append = False
				if end_of_line:
					pass
				elif start_of_string:
					if c == "\"":
						value += c
						start_of_string = False
						append = True
					else:
						value += c
				else:
					if first_slash and c != "/":
						first_slash = False
						value += "$["+c+"]"
					elif first_slash: #start of comment
						end_of_line = True
						first_slash = False
						append = True
					elif c == "=": #start of value, end of var name
						var = value.strip()
						value = ""
					elif c == "/": #start of comment?
						#first_slash = True
						value += c
					elif c == "\"": #start of string
						start_of_string = True
						value += c
					elif c == "{": #start of list
						append_list = True
					elif c == "}":
						append = True
					elif c == ",": #separator of list
						pass
					elif c == ";":
						end_of_line = True
						append = True
						append_list = False
					else:
						value += c
			
				if append:
					self.appendValue(value, values)
					value = ""

				if end_of_line and c == "\n":
					end_of_line = False

			self.appendValue(value, values)
			if var:
				if append_list:
					parameters[var] = values
				else:
					parameters[var] = values[0]

		if not "RootConfigFilename" in parameters and rootConfigFilename:
			parameters["RootConfigFilename"] = rootConfigFilename

		return parameters

	def printvar(self, var):
		return var+" "*(20-len(var))

	def save(self):
		final = []
		if not "RootConfigFilename" in self.content:
			self.set("RootConfigFilename", self.rootconfig)

		final.append("RootConfigFilename   = \""+self.get("RootConfigFilename")+"\"") # Very important and first line
		for var, value in self.content.items():
			if var != "RootConfigFilename":
				if type(value) == str:
					final.append(self.printvar(var)+" = \""+value+"\"")
				elif type(value) == int:
					final.append(self.printvar(var)+" = {}".format(value))
				elif type(value) == float:
					final.append(self.printvar(var)+" = {}".format(value, ".10f"))
				else:
					finalval = "\t"
					for i in range(len(value)):
						if i == len(value)-1:
							finalval += "\""+value[i]+"\" "
						else:
							finalval += "\""+value[i]+"\", "
						if (i+1) % 6 == 0:
							finalval += "\n\t"
					final.append(self.printvar(var)+" = {\n"+finalval+"\n}")
		with open(self.filename, "w") as f:
			f.write(";\n".join(final)+";")

	def empty(self):
		return len(self.content) == 0

	def get(self, var, default=None):
		if not var in self.content:
			return default
		
		return self.content[var]

	def set(self, var, value):
		self.log("Set {} = {}".format(var, value))
		self.content[var] = value

	def remove(self, var):
		del(self.content[var])

	def printAll(self):
		print(self.content)

class RyzomCfg(KyssModule):
	_configs = {}
	
	def getCfg(self, filename, rootConfigFilename=""):
		if not filename in RyzomCfg._configs:
			RyzomCfg._configs[filename] = RyzomCfgFile(filename, rootConfigFilename)
		if rootConfigFilename != "":
			RyzomCfg._configs[filename].rootconfig = rootConfigFilename
		return RyzomCfg._configs[filename]
		
		
		
		
		
		
